import numpy as np
import tensorflow as tf
from transformers import BertTokenizer, TFAutoModelForSequenceClassification
from TextPreprocessing import TextPreprocessing


MODEL_PATH = './model/'
TOKENIZER_PATH = './tokenizer/'
model = TFAutoModelForSequenceClassification.from_pretrained(MODEL_PATH)
tokenizer = BertTokenizer.from_pretrained(TOKENIZER_PATH)
text_preprocessing = TextPreprocessing()


def predict(text: str) -> dict:
    # preproeses text
    text_preprocessed = text_preprocessing.full_preprocessing_with_normalization(text)
    # tokenisasi
    input_token = tokenizer.encode(text_preprocessed, truncation=True, padding=True, max_length=128, return_tensors="tf")
    # hasil berupa logits dalam bentuk one-hot encoding
    output_logits = model.predict(input_token)[0]
    # mendapatkan probabilitas
    prediction = tf.nn.softmax(output_logits, axis=1).numpy()[0]
    # mendapatkan label prediksi
    prediction_label_id = np.argmax(np.round(prediction)) + 1 # ditambah 1 karena start dari 0

    if prediction_label_id == 1:
        prediction_label = 'not abusive language'
    elif prediction_label_id == 2:
        prediction_label = 'abusive but not offensive'
    elif prediction_label_id == 3:
        prediction_label = 'offensive language'

    prediction_dict = {
        # 'text' : text,
        'label_id' : str(prediction_label_id),
        'label' : prediction_label,
        'probability' : str(prediction.max())
        # 'tes' : 'tes'
    }

    return prediction_dict

if __name__ == "__main__":
    # text = 'bangsat komentarmu kek orang gak berpendidikan kontol'
    text = "gua lupa ga bawa dompet bangsat"
    result = predict(text)
    print(result)