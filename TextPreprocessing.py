import re
import pandas as pd

class TextPreprocessing:
    def __init__(self):
        self.kamus_alay_path = './kamus/kamusalay.xlsx'
        self.kamus_alay = pd.read_excel(self.kamus_alay_path, names=['alay', 'normal'])


    # menghapus simbol-simbol non alphabet
    def remove_symbol(self, text: str) -> str:
        # remove html entities
        text_no_html_ents = re.sub("&.+;", '', text)
        # remove newline
        text_no_newline = text_no_html_ents.replace(r"\n", " ")
        # remove symbol
        result = re.sub(r'\W+', ' ', text_no_newline)
        
        return result


    # menghapus emoji
    def remove_emoji(self, text: str) -> str:
        # remove emojis
        result = re.sub("x[a-zA-Z0-9][a-zA-Z0-9]", '', text)
        
        return result


    # menghapus html attribute
    def remove_html_atts(self, text: str) -> str:
        result = re.sub('URL|USER|RT', '', text)

        return result


    # menghapus spasi
    def remove_whitespaces(self, text: str) -> str:
        # remove multi white space
        result = re.sub(" +", " ", text)
        # remove whitespaces di awal
        result = result.lstrip()
        # remove whitespaces di belakang
        result = result.rstrip()

        return result
    

    # menghapus angka numerik
    def remove_number(self, text):
        result = re.sub(r'\d+', '', text)
        return result
    

    # normalisasi kata
    def normalize(self, text: str):
        text = text.split(' ')
        text_normal = []
    
        for word in text:
            result = self.kamus_alay[self.kamus_alay['alay'] == word]
            # ada
            if len(result) > 0:
                normal_word = result['normal'].values[0]
                text_normal.append(normal_word)
            # tidak ada
            else:
                text_normal.append(word)
        
        return ' '.join(text_normal)
    

    # full preprocessing tanpa normalisasi kata
    def full_preprocessing(self, text: str) -> str:
        text_no_symbol = self.remove_symbol(text)
        text_no_emoji = self.remove_emoji(text_no_symbol)
        text_no_html_atts = self.remove_html_atts(text_no_emoji)
        text_no_number = self.remove_number(text_no_html_atts)
        text_no_multispaces = self.remove_whitespaces(text_no_number)
        text_lower = text_no_multispaces.lower()

        return text_lower
    

    # full preprocessing dengan normalisasi kata
    def full_preprocessing_with_normalization(self, text: str) -> str:
        text_no_symbol = self.remove_symbol(text)
        text_no_emoji = self.remove_emoji(text_no_symbol)
        text_no_html_atts = self.remove_html_atts(text_no_emoji)
        text_no_number = self.remove_number(text_no_html_atts)
        text_no_multispaces = self.remove_whitespaces(text_no_number)
        text_lower = text_no_multispaces.lower()
        text_normalized = self.normalize(text_lower)

        return text_normalized